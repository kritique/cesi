# Partie 3 : Maintien en condition opérationnelle

## Sommaire

- [Partie 3 : Maintien en condition opérationnelle](#partie-3--maintien-en-condition-opérationnelle)
  - [Sommaire](#sommaire)
- [I. Monitoring](#i-monitoring)
  - [1. Intro](#1-intro)
  - [2. Setup Netdata](#2-setup-netdata)
  - [3. Bonus : alerting](#3-bonus--alerting)
  - [4. Bonus : proxying](#4-bonus--proxying)
- [II. Backup](#ii-backup)

# I. Monitoring


## 2. Setup Netdata

🌞 **Installez Netdata** en exécutant la commande suivante :

```bash
[lucas@prxoy ~]$ bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)    --- Found existing install of Netdata under: /opt/netdata ---
 --- Attempting to update existing install instead of creating a new one ---
[/home/lucas]$ sudo /opt/netdata/usr/libexec/netdata/netdata-updater.sh --not-running-from-cron

```


🌞 **Démarrez Netdata**

```bash
[lucas@prxoy ~]$ sudo systemctl start netdata

[lucas@prxoy ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
Warning: ALREADY_ENABLED: 19999:tcp
success

$ curl 10.2.1.13:19999
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0<!doctype html><html lang="en"><head><title>netdata dashboard</title><meta name="application-name" content="netdata"><meta http-equiv="Content-Type"
  [...]
```

## 3. Bonus : alerting

🌞 **Mettez en place un alerting Discord**

- [lien de la doc](https://learn.netdata.cloud/docs/agent/health/notifications/discord) pour l'alerting Discord
- le principe est de recevoir les alertes dans un salon texte dédié sur un serveur Discord

## 4. Bonus : proxying

🌞 **Ajustez la conf**

- l'interface web de Netdata doit être joignable par le reverse proxy

# II. Backup

[Borg](https://borgbackup.readthedocs.io/en/stable/) est un outil qui permet de réaliser des sauvegardes.

Il est performant et sécurisé :

- performant à l'utilisation
  - création de backup rapide, idem pour restauration
- les données sauvegardées sont chiffrées
- les données sauvegardées sont dédupliquées

Il repose sur un concept simple :

- on créer un "dépôt Borg" sur la machine avec une commande `borg init`
- c'est dans ce dépôt que pourront être stockées des sauvegardes
- on peut ensuite déclencher une sauvegarde d'un dossier donné vers le dépôt avec une commande `borg create`

Nous, on va écrire un script :

- il créer une nouvelle sauvegarde borg (avec `borg create` donc) d'un dossier précis, avec un nommage précis, dans un dépôt précis
- on en fera ensuite un service : un p'tit `backup.service`
- puis on fera en sorte que ce service se lance à intervalles réguliers

🌞 **Téléchargez et jouez avec Borg** sur la machine `web.tp2.cesi`
```bash
[lucas@prxoy ~]$ sudo pip3 install -U pip setuptools wheel
WARNING: Running pip install with root privileges is generally not a good idea. Try `pip3 install --user` instead.
Collecting pip
  Downloading https://files.pythonhosted.org/packages/a4/6d/6463d49a933f547439d6b5b98b46af8742cc03ae83543e4d7688c2420f8b/pip-21.3.1-py3-none-any.whl (1.7MB)
    100% |████████████████████████████████| 1.7MB 1.1MB/s
Collecting setuptools
  Downloading https://files.pythonhosted.org/packages/40/a9/7deac76c58fa47c95360116a06b53b9b62f6db11336fe61b6ab53784d98b/setuptools-59.5.0-py3-none-any.whl (952kB)
    100% |████████████████████████████████| 962kB 1.8MB/s
Collecting wheel
  Downloading https://files.pythonhosted.org/packages/04/80/cad93b40262f5d09f6de82adbee452fd43cdff60830b56a74c5930f7e277/wheel-0.37.0-py2.py3-none-any.whl
Installing collected packages: pip, setuptools, wheel
Successfully installed pip-21.3.1 setuptools-59.5.0 wheel-0.37.0

[lucas@prxoy ~]$ sudo cp borg-linux64 /usr/local/bin/borg
[lucas@prxoy ~]$ sudo chown root:root /usr/local/bin/borg
[lucas@prxoy ~]$ sudo chmod 755 /usr/local/bin/borg
[lucas@prxoy ~]$

``` 


