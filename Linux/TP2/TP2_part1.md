
## Sommaire

- [Partie 1 : Mise en place de la solution](#partie-1--mise-en-place-de-la-solution)
  - [Sommaire](#sommaire)
- [I. Setup base de données](#i-setup-base-de-données)
  - [1. Install MariaDB](#1-install-mariadb)
  - [2. Conf MariaDB](#2-conf-mariadb)
  - [3. Test](#3-test)
- [II. Setup Apache](#ii-setup-apache)
  - [1. Install Apache](#1-install-apache)
    - [A. Apache](#a-apache)
    - [B. PHP](#b-php)
  - [2. Conf Apache](#2-conf-apache)
- [III. NextCloud](#iii-nextcloud)
  - [4. Test](#4-test)

# I. Setup base de données

## 1. Install MariaDB

🌞 **Installer MariaDB sur la machine `db.tp2.cesi`**

```bash
[lucas@db ~]$ sudo dnf install mariadb-server -y
Last metadata expiration check: 0:27:20 ago on Tue 07 Dec 2021 01:46:30 PM CET.
Dependencies resolved.
[...]
```

🌞 **Le service MariaDB**
```bash
[lucas@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
```

```bash
[lucas@db ~]$ sudo systemctl status mariadb
mariadb.service - MariaDB 10.3 database server
Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
Active: active (running) since Tue 2021-12-07 14:16:49 CET; 1min 2s ago
```
```bash
[lucas@db ~]$ sudo ss -lutpn
tcp       LISTEN     0          80                         *:3306                    *:*        users:(("mysqld",pid=3033,fd=21))
```
```bash
[lucas@db ~]$ ps
    PID TTY          TIME CMD
   1667 pts/0    00:00:00 bash
   3116 pts/0    00:00:00 ps
``` 

🌞 **Firewall**

```bash
[lucas@db ~]$ sudo firewall-cmd --add-port=3306/tcp
success
```


## 2. Conf MariaDB

Première étape : le `mysql_secure_installation`. C'est un binaire (= une commande, une application, un programme, ces mots désignent la même chose) qui sert à effectuer des configurations très récurrentes, on fait ça sur toutes les bases de données à l'install.  
C'est une question de sécu.

🌞 **Configuration élémentaire de la base**

```bash
All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```

---

🌞 **Préparation de la base en vue de l'utilisation par NextCloud**


```bash
[lucas@db ~]$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 16
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>

```

Puis, dans l'invite de commande SQL :

```sql
CREATE USER 'nextcloud'@'10.2.1.11' IDENTIFIED BY 'meow';
MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.2.1.11';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> quit
Bye

```

## 3. Test


🌞 **Installez sur la machine `web.tp2.cesi` la commande `mysql`**

```bash
sudo dnf install mysql
[sudo] password for lucas:
Last metadata expiration check: 1:02:00 ago on Tue 07 Dec 2021 01:46:30 PM CET.
Dependencies resolved.
```

🌞 **Tester la connexion**

```bash
[lucas@web ~]$ mysql -h 10.2.1.12 -u nextcloud -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 23
Server version: 5.5.5-10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> use nextcloud;
Database changed
mysql> show tables;
Empty set (0.00 sec)
```

---


# II. Setup Apache

> La section II et III sont clairement inspirés de [la doc officielle de Rocky pour installer NextCloud](https://docs.rockylinux.org/guides/cms/cloud_server_using_nextcloud/).

Comme annoncé dans l'intro, on va se servir d'Apache dans le rôle de serveur Web dans ce TP5. Histoire de varier les plaisirs è_é

![Linux is a tipi](./pics/linux_is_a_tipi.jpg)

## 1. Install Apache

### A. Apache

🌞 **Installer Apache sur la machine `web.tp2.cesi`**
```bash
[lucas@web ~]$ sudo dnf install httpd
[sudo] password for lucas:
Last metadata expiration check: 1:11:18 ago on Tue 07 Dec 2021 01:46:30 PM CET.
Dependencies resolved.
``` 
---

🌞 **Analyse du service Apache**
```bash
[lucas@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-07 14:59:24 CET; 17s ago
   
 [lucas@web ~]$ sudo ss -lutpn
  tcp           LISTEN          0               128                                  *:80                                *:*             users:(("httpd",pid=2706,fd=4),("httpd",pid=2705,fd=4),("httpd",pid=2704,fd=4),("httpd",pid=2702,fd=4))
  ```
  
---

🌞 **Un premier test**

```bash
 [lucas@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens160 ens224
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 2021/tcp 2022/tcp 80/tcp 8888/tcp

C:\Users\LUCAS>curl 10.2.1.11
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
[...]
```

### B. PHP

NextCloud a besoin d'une version bien spécifique de PHP.  
Suivez **scrupuleusement** les instructions qui suivent pour l'installer.

🌞 **Installer PHP**

```bash
[lucas@web ~]$ sudo dnf install epel-release
Last metadata expiration check: 1:19:02 ago on Tue 07 Dec 2021 01:46:30 PM CET.
Dependencies resolved.

[lucas@web ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
Extra Packages for Enterprise Linux 8 - x86_64                                                        2.1 MB/s |  11 MB     00:05
Extra Packages for Enterprise Linux Modular 8 - x86_64                                                712 kB/s | 980 kB     00:01
remi-release-8.rpm                                                                                     87 kB/s |  26 kB     00:00
Dependencies resolved.

sudo dnf module enable php:remi-7.4

sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
```

## 2. Conf Apache

➜ Le fichier de conf principal utilisé par Apache est `/etc/httpd/conf/httpd.conf`.  
Il y en a plein d'autres : ils sont inclus par le fichier de conf principal.

➜ Dans Apache, il existe la notion de *VirtualHost*. On définit des *VirtualHost* dans les fichiers de conf d'Apache.  
On crée un *VirtualHost* pour chaque application web qu'héberge Apache.

> "Application Web" c'est le terme de hipster pour désigner un site web. Disons qu'aujourd'hui les sites peuvent faire tellement de trucs qu'on appelle plutôt ça une "application" plutôt qu'un "site". Une application web donc.

➜ Dans le dossier `/etc/httpd/` se trouve un dossier `conf.d`.  
Des dossiers qui se terminent par `.d`, vous en rencontrerez plein (pas que pour Apache) **ce sont des dossiers de *drop-in*.**

Plutôt que d'écrire 40000 lignes dans un seul fichier de conf, on éclate la configuration dans plusieurs fichiers.  
C'est + lisible et + facilement maintenable.

**Les dossiers de *drop-in* servent à accueillir ces fichiers de conf additionels.**  
Le fichier de conf principal a une ligne qui inclut tous les autres fichiers de conf contenus dans le dossier de *drop-in*.

---

🌞 **Analyser la conf Apache**

```bash
IncludeOptional conf.d/*.conf
```

🌞 **Créer un VirtualHost qui accueillera NextCloud**

```bash
[lucas@web ~]$ sudo nano /etc/httpd/conf.d/nextcloud.conf
```

```apache
<VirtualHost *:80>
  # on précise ici le dossier qui contiendra le site : la racine Web
  DocumentRoot /var/www/html/nextcloud

  # ici le nom qui sera utilisé pour accéder à l'application
  ServerName  web.tp2.cesi  

  <Directory /var/www/html/nextcloud>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```


🌞 **Configurer la racine web**

```bash
sudo mkdir /var/www/html/nextcloud

[lucas@web html]$ sudo chown -R apache /var/www/html/nextcloud/
[lucas@web html]$ ls -al /var/www/html/
total 0
drwxr-xr-x. 3 root   root 23 Dec  7 15:21 .
drwxr-xr-x. 4 root   root 33 Dec  7 14:58 ..
drwxr-xr-x. 2 apache root  6 Dec  7 15:21 nextcloud
[lucas@web html]$
```


🌞 **Configurer PHP**

```bash
[lucas@web html]$ timedatectl
               Local time: Tue 2021-12-07 15:25:49 CET
           Universal time: Tue 2021-12-07 14:25:49 UTC
                 RTC time: Tue 2021-12-07 14:25:48
                Time zone: Europe/Paris (CET, +0100)
System clock synchronized: yes
              NTP service: active
          RTC in local TZ: no
[lucas@web html]$ cat /etc/opt/remi/php74/php.
php.d/   php.ini
[lucas@web html]$ cat /etc/opt/remi/php74/php.ini | grep date.timezone
; http://php.net/date.timezone
date.timezone = "Europe/Paris"
[lucas@web html]$
```

# III. NextCloud

On dit "installer NextCloud" mais en fait c'est juste récupérer les fichiers PHP, HTML, JS, etc... qui constituent NextCloud, et les mettre dans le dossier de la racine web.

🌞 **Récupérer Nextcloud**

```bash
[lucas@web ~]$ ls
nextcloud-21.0.1.zip  ranger

```

🌞 **Ranger la chambre**
```bash
[lucas@web ~]$ unzip nextcloud-21.0.1.zip

[lucas@web ~]$ ls
nextcloud  nextcloud-21.0.1.zip  ranger

[lucas@web nextcloud]$ ls
3rdparty  AUTHORS  console.php  core      index.html  lib  ocm-provider  ocs-provider  remote.php  robots.txt  themes   version.php
apps      config   COPYING      cron.php  index.php   occ  ocs           public.php    resources   status.php  updater
[lucas@web nextcloud]$ sudo chown -R apache /var/www/html/nextcloud/
[lucas@web nextcloud]$ ls -al
total 120
drwxr-xr-x. 13 apache root   4096 Dec  7 15:34 .
drwxr-xr-x.  3 root   root     23 Dec  7 15:21 ..
drwxr-xr-x. 43 apache lucas  4096 Apr  8  2021 3rdparty
drwxr-xr-x. 47 apache lucas  4096 Apr  8  2021 apps
-rw-r--r--.  1 apache lucas 17900 Apr  8  2021 AUTHORS
drwxr-xr-x.  2 apache lucas    67 Apr  8  2021 config
-rw-r--r--.  1 apache lucas  3900 Apr  8  2021 console.php
-rw-r--r--.  1 apache lucas 34520 Apr  8  2021 COPYING
drwxr-xr-x. 22 apache lucas  4096 Apr  8  2021 core
-rw-r--r--.  1 apache lucas  5122 Apr  8  2021 cron.php
-rw-r--r--.  1 apache lucas   156 Apr  8  2021 index.html
-rw-r--r--.  1 apache lucas  2960 Apr  8  2021 index.php
drwxr-xr-x.  6 apache lucas   125 Apr  8  2021 lib
-rw-r--r--.  1 apache lucas   283 Apr  8  2021 occ
drwxr-xr-x.  2 apache lucas    23 Apr  8  2021 ocm-provider
drwxr-xr-x.  2 apache lucas    55 Apr  8  2021 ocs
drwxr-xr-x.  2 apache lucas    23 Apr  8  2021 ocs-provider
-rw-r--r--.  1 apache lucas  3144 Apr  8  2021 public.php
-rw-r--r--.  1 apache lucas  5341 Apr  8  2021 remote.php
drwxr-xr-x.  4 apache lucas   133 Apr  8  2021 resources
-rw-r--r--.  1 apache lucas    26 Apr  8  2021 robots.txt
-rw-r--r--.  1 apache lucas  2446 Apr  8  2021 status.php
drwxr-xr-x.  3 apache lucas    35 Apr  8  2021 themes
drwxr-xr-x.  2 apache lucas    43 Apr  8  2021 updater
-rw-r--r--.  1 apache lucas   382 Apr  8  2021 version.php
``` 

## 4. Test

Bah on arrive sur la fin du setup !

Si on résume :

- **un serveur de base de données : `db.tp2.cesi`**
  - MariaDB installé et fonctionnel
  - firewall configuré
  - une base de données et un user pour NextCloud ont été créés dans MariaDB
- **un serveur Web : `web.tp2.cesi`**
  - Apache installé et fonctionnel
  - firewall configuré
  - un VirtualHost qui pointe vers la racine `/var/www/nextcloud/html/`
  - NextCloud installé dans le dossier `/var/www/nextcloud/html/`

**Looks like we're ready.**

---


🌞 **Modifiez le fichier `hosts` de votre PC**

```
# localhost name resolution is handled within DNS itself.
#	127.0.0.1       localhost
#	::1             localhost
10.2.1.11 web.tp2.cesi
```

🌞 **Tester l'accès à NextCloud et finaliser son install'**

```html
C:\Users\LUCAS>curl http://web.tp2.cesi
<!DOCTYPE html>
<html>
<head>
        <script> window.location.href="index.php"; </script>
        <meta http-equiv="refresh" content="0; URL=index.php">
</head>
</html>
```

```
C:\Users\LUCAS>curl http://web.tp2.cesi/index.php/apps/dashboard/
{"message":"Current user is not logged in"}
```