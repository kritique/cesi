# Partie 2 : Sécurisation


## Sommaire

- [Partie 2 : Sécurisation](#partie-2--sécurisation)
  - [Sommaire](#sommaire)
- [I. Serveur SSH](#i-serveur-ssh)
  - [1. Conf SSH](#1-conf-ssh)
  - [2. Bonus : Fail2Ban](#2-bonus--fail2ban)
- [II. Serveur Web](#ii-serveur-web)
  - [1. Reverse Proxy](#1-reverse-proxy)
  - [2. HTTPS](#2-https)

# I. Serveur SSH


## 1. Conf SSH

Si c'est pas fait vous **devez maintenant** configurer un accès par clés pour SSH.

🌞 **Modifier la conf du serveur SSH**
```bash
[lucas@web ~]$ sudo cat /etc/ssh/sshd_config | grep -i 'root'
PermitRootLogin no

[lucas@web ~]$ sudo cat /etc/ssh/sshd_config | grep PasswordAuthentication
#PasswordAuthentication yes
PasswordAuthentication no
# PasswordAuthentication.  Depending on your PAM configuration,
# PAM authentication, then enable this but set PasswordAuthentication

[lucas@web ~]$ sudo cat /etc/ssh/sshd_config | grep algorithms
[sudo] password for lucas:
# algorithms
hostkeyalgorithms ecdsa-sha2-nistp256-cert-v01@openssh.com,ecdsa-sha2-nistp384-cert-v01@openssh.com,ecdsa-sha2-nistp521-cert-v01@openssh.com,ssh-ed25519-cert-v01@openssh.com,rsa-sha2-512-cert-v01@openssh.com,rsa-sha2-256-cert-v01@openssh.com,ssh-rsa-cert-v01@openssh.com,ecdsa-sha2-nistp384,ecdsa-sha2-nistp521,ssh-ed25519,rsa-sha2-512,rsa-sha2-256

```

## 2. Bonus : Fail2Ban

🌞 **Installez et configurez fail2ban**
```bash
[lucas@web ~]$ sudo systemctl enable fail2ban
Created symlink /etc/systemd/system/multi-user.target.wants/fail2ban.service → /usr/lib/systemd/system/fail2ban.service.
[lucas@web ~]$ sudo systemctl start fail2ban
[lucas@web ~]$ sudo systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
   Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; enabled; vendor pr>
   Active: active (running) since Wed 2021-12-08 09:26:33 CET; 6s ago

[lucas@db ~]$ ssh 10.2.1.11 -p 2022
lucas@10.2.1.11: Permission denied (publickey,gssapi-keyex,gssapi-with-mic).

```

# II. Serveur Web

Bah ui certains l'ont fait en bonus hier, mais comme ça, on met tout le monde à niveau. Libre à vous de sauter ça ou de le refaire (un peu plus en conscience avec le cours sur TLS ?) ou simplement de le faire si c'était pas fait avant !

## 1. Reverse Proxy


🌞 **Installer NGINX**
```bash
[lucas@prxoy ~]$ sudo dnf install nginx
[sudo] password for lucas:
Last metadata expiration check: 20:29:33 ago on Tue 07 Dec 2021 01:46:30 PM CET.
Package nginx-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64 is already installed.
Dependencies resolved.
``` 
**Quelques infos :**

- les fichiers de log sont dans le dossier `/var/log/nginx/`
- la conf principale est dans `/etc/nginx/nginx.conf`
  - une des dernières lignes inclut des fichiers contenus dans `/etc/nginx/conf.d/`
  - n'hésitez pas à le lire le fichier, et à l'épurer des commentaires inutiles pour y voir plus clair
- n'oubliez pas la commande `sudo journalctl -xe -u <SERVICE>` pour obtenir les logs d'un service donné

🌞 **Configurer NGINX comme reverse proxy**
```bash
[lucas@prxoy ~]$ sudo cat /etc/nginx/conf.d/reverse_proxy
server {
    listen 80;
    server_name web1.tp2.cesi;

    location / {
    proxy_pass http://10.2.1.11:80;
    proxy_http_version  1.1;
    proxy_cache_bypass  $http_upgrade;

    proxy_set_header Upgrade           $http_upgrade;
    proxy_set_header Connection        "upgrade";
    proxy_set_header Host              $host;
    proxy_set_header X-Real-IP         $remote_addr;
    proxy_set_header X-Forwarded-For   $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header X-Forwarded-Host  $host;
    proxy_set_header X-Forwarded-Port  $server_port;

    }
}
```

🌞 **Une fois en place, text !**

- il faudra de nouveau modifier le fichier hosts de votre PC : bah oui, le nom `web.tp2.cesi` doit pointer vers l'IP du reverse proxy maintenant !
  - mais mais mais, `web.tp2.cesi` qui pointe vers l'IP d'une autre machine, le reverse proxy, c'est pas chelou ?
  - bah non :D

## 2. HTTPS

🌞 **Générer une clé et un certificat avec la commande suivante :**

```bash
[lucas@prxoy ~]$ sudo openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -keyout server.key -out server.crt
Generating a RSA private key
...............................................++++
.++++
writing new private key to 'server.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:FR
State or Province Name (full name) []:France
Locality Name (eg, city) [Default City]:Bordeaux
Organization Name (eg, company) [Default Company Ltd]:Info
Organizational Unit Name (eg, section) []:Info
Common Name (eg, your name or your server's hostname) []:web.tp2.cesi
Email Address []:lucas.longobardi@gmail.com
```

**Par convention dans les systèmes RedHat :**


🌞 **Allez, faut ranger la chambre**
```bash
[lucas@prxoy ~]$ sudo mv server.crt /etc/pki/tls/private/
[lucas@prxoy ~]$ sudo mv server.key /etc/pki/tls/certs/

[lucas@prxoy tls]$ ls private/
web.tp2.cesi.key
[lucas@prxoy tls]$ ls certs/
ca-bundle.crt  ca-bundle.trust.crt  web.tp2.cesi.key
[lucas@prxoy tls]$
```

🌞 **Affiner la conf de NGINX**


```nginx
[lucas@prxoy tls]$ sudo cat /etc/nginx/conf.d/reverse_proxy.conf
server {
    listen 443 ssl http2;
    server_name web.tp2.cesi;
    ssl_certificate /etc/pki/tls/private/web.tp2.cesi.key;
    ssl_certificate_key /etc/pki/tls/certs/web.tp2.cesi.cert;


    location / {
    proxy_pass http://10.2.1.11;
    proxy_http_version  1.1;
    proxy_cache_bypass  $http_upgrade;

    proxy_set_header Upgrade           $http_upgrade;
    proxy_set_header Connection        "upgrade";
    proxy_set_header Host              $host;
    proxy_set_header X-Real-IP         $remote_addr;
    proxy_set_header X-Forwarded-For   $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header X-Forwarded-Host  $host;
    proxy_set_header X-Forwarded-Port  $server_port;
    }

}
```

Eeet bah c'est tout.

🌞 **Test !**
```bash
$ curl -k https://web.tp2.cesi -L
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0
100  3332  100  3332    0     0  56647      0 --:--:-- --:--:-- --:--:-- 56647<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
```

🌞 **Bonus**
```bash
server {
    listen 80;
    server_name web.tp2.cesi;
    return 301 https://web.tp2.cesi$request_uri;
}


server {
    listen 443 ssl http2;
    server_name web.tp2.cesi;
    ssl_certificate /etc/pki/tls/private/web.tp2.cesi.key;
    ssl_certificate_key /etc/pki/tls/certs/web.tp2.cesi.cert;

    location / {
    proxy_pass http://10.2.1.11;
    proxy_http_version  1.1;
    proxy_cache_bypass  $http_upgrade;

    proxy_set_header Upgrade           $http_upgrade;
    proxy_set_header Connection        "upgrade";
    proxy_set_header Host              $host;
    proxy_set_header X-Real-IP         $remote_addr;
    proxy_set_header X-Forwarded-For   $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header X-Forwarded-Host  $host;
    proxy_set_header X-Forwarded-Port  $server_port;

    }

}
``` 
- **renforcer l'échange TLS en sélectionnant les algos de chiffrement à utiliser**
  - cherchez "nginx strong ciphers" :)
- **répartition de charge**
  - clonez la VM Apache + NextCloud
  - mettez en place de la répartition de charge entre les deux machines renommées `web1.tp2.cesi` et `web2 .tp2.cesi`

